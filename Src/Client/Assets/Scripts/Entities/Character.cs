﻿using Entities;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Entities
{
    public class Character : Entity
    {
        public NCharacterInfo info;

        public Common.Data.CharacterDefine define;

        public int Id
        {
            get
            {
                return this.info.Id;
            }
        }

        public string Name
        {
            get
            {
                if (this.info.Type == CharacterType.Player)
                {
                    return this.info.Name;
                }
                else
                {
                    return this.define.Name;
                }
            }
        }

        public bool IsPlayer
        {
            // get { return this.info.Id == Models.User.Instance.CurrentCharacter.Id; }
            get
            {
                return this.info.Type == CharacterType.Player;
            }
        }

        public bool IsCurrentPlayer
        {
            get
            {
                if (!IsPlayer)
                {
                    return false;
                }
                return this.info.Id == Models.User.Instance.CurrentCharacter.Id;
            }
        }

        public Character(NCharacterInfo info) : base(info.Entity)
        {
            this.info = info;
            this.define = DataManager.Instance.Characters[info.ConfigId];
        }

        public void MoveForward()
        {
            Debug.LogFormat("MoveForward");
            this.speed = this.define.Speed;
        }

        public void MoveBack()
        {
            Debug.LogFormat("MoveBack");
            this.speed = -this.define.Speed;
        }

        public void Stop()
        {
            Debug.LogFormat("Stop");
            this.speed = 0;
        }

        public void SetDirection(Vector3Int direction)
        {
            Debug.LogFormat("SetDirection:{0}", direction);
            this.direction = direction;
        }

        public void SetPosition(Vector3Int position)
        {
            Debug.LogFormat("SetPosition:{0}", position);
            this.position = position;
        }
    }
}
