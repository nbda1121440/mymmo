﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    void Start()
    {
    }

    private void Update()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        float x = transform.position.x + v * Time.deltaTime;
        float z = transform.position.z + h * Time.deltaTime;
        transform.position = new Vector3(x, transform.position.y, z);
    }
}
