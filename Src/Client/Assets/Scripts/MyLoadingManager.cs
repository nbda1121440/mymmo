﻿using Managers;
using Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyLoadingManager : MonoBehaviour {

    public GameObject UILoading;
    public GameObject 进度条页面;
    public GameObject 游戏健康提醒页面;
    public GameObject UILogin;

    public Text 进度条文字;
    public Slider 进度条;

	// Use this for initialization
	IEnumerator Start () {
        log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo("log4net.xml"));
        UnityLogger.Init();
        Common.Log.Init("Unity");
        Common.Log.Info("MyLoadingManager start");

        // 首先展示 游戏健康提醒 页面
        UILoading.SetActive(true);
        进度条页面.SetActive(false);
        游戏健康提醒页面.SetActive(true);
        UILogin.SetActive(false);
        yield return new WaitForSeconds(2f);

        // 这里需要加载数据
        yield return DataManager.Instance.LoadData();

        // 初始化基础服务
        MapService.Instance.Init();
        UserService.Instance.Init();
        StatusService.Instance.Init();
        ShopManager.Instance.Init();
        FriendService.Instance.Init();
        TeamService.Instance.Init();
        GuildService.Instance.Init();
        ChatService.Instance.Init();

        // TestManager.Instance.Init();

        // 因为 游戏健康提醒 页面2秒后会因为动画淡出，所以这时再显示进度条页面，让进度条从0慢慢增加到100
        进度条页面.SetActive(true);
        for (int i = 0; i <= 100; i++)
        {
            进度条文字.text = i + "%";
            进度条.value = i;
            yield return new WaitForSeconds(0.01f);
        }
        // 到 100% 的时候等待 1 秒中
        yield return new WaitForSeconds(1.0f);

        // 接着隐藏进度条页面，显示登录界面
        UILoading.SetActive(false);
        UILogin.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
