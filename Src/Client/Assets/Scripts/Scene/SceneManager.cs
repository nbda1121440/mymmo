﻿using Managers;
using Models;
using SkillBridge.Message;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SceneManager : MonoSingleton<SceneManager>
{
    UnityAction<float> onProgress = null;
    List<NCharacterInfo> nCharacterInfos = null;

    // Use this for initialization
    protected override void OnStart()
    {
        
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void LoadScene(string name)
    {
        StartCoroutine(LoadLevel(name));
    }

    public void LoadScene(string name, List<NCharacterInfo> nCharacterInfos)
    {
        this.nCharacterInfos = nCharacterInfos;
        StartCoroutine(LoadLevel(name));
    }

    IEnumerator LoadLevel(string name)
    {
        Debug.LogFormat("LoadLevel: {0}", name);
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(name);
        async.allowSceneActivation = true;
        async.completed += LevelLoadCompleted;
        while (!async.isDone)
        {
            if (onProgress != null)
                onProgress(async.progress);
            yield return null;
        }
    }

    private void LevelLoadCompleted(AsyncOperation obj)
    {
        if (onProgress != null)
            onProgress(1f);

        Debug.Log("LevelLoadCompleted:" + obj.progress);

        if (nCharacterInfos != null)
        {
            // 场景加载完毕之后，再把角色加载一遍
            foreach (var cha in nCharacterInfos)
            {
                if (User.Instance.CurrentCharacter == null || User.Instance.CurrentCharacter.Id == cha.Id)
                {
                    // 当前角色切换地图
                    // 这里再赋一次值是在登录期间，服务器可能给这个角色发了一些经验，所以再同步一次该角色的数据会更好一点
                    User.Instance.CurrentCharacter = cha;
                }
                CharacterManager.Instance.AddCharacter(cha);
            }

            nCharacterInfos = null;
        }
    }
}
