﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities
{
    class TimeUtil
    {
        internal static DateTime GetTime(long joinTime)
        {
            return DateTime.FromFileTimeUtc(joinTime);
        }
    }
}
