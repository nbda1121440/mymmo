﻿using Models;
using Services;
using SkillBridge.Message;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMyCharSelect : MonoBehaviour {

    public GameObject prefabCharInfo;
    public Transform uiCharList;
    public MyCharacterView myCharacterView;
    private int selectCharacterId = -1;
    private int selectCharacterIdx = -1;

    // Use this for initialization
    void Start () {
        RenewCharacterList();
    }

    public void RenewCharacterList()
    {
        // 首先清空角色信息列表下面所有的项目
        for (int i = 0; i < uiCharList.childCount; i++)
        {
            Destroy(uiCharList.GetChild(i).gameObject);
        }
        // 将登录时用户上面的角色列表显示在 uiCharList 上面
        List<NCharacterInfo> list = User.Instance.Info.Player.Characters;
        for (int i = 0; i < list.Count; i++)
        {
            NCharacterInfo info = list[i];

            GameObject go = Instantiate(prefabCharInfo, uiCharList);
            MyCharInfo myCharInfo = go.GetComponent<MyCharInfo>();
            myCharInfo.CharInfo = info;
            myCharInfo.index = i;

            Button button = go.GetComponent<Button>();
            int idx = i;
            button.onClick.AddListener(() =>
            {
                OnSelectCharacter(idx);
            });
        }

        if (list.Count == 0)
        {
            // 如果列表本身就是空的，那么不显示角色
            myCharacterView.SetChar(-1);
        }
        else
        {
            // 如果列表不是空的，就显示第一个角色
            OnSelectCharacter(0);
        }
    }

    public void OnSelectCharacter(int idx)
    {
        this.selectCharacterIdx = idx;
        var cha = User.Instance.Info.Player.Characters[idx];
        Debug.LogFormat("Select Char:[{0}]{1}[{2}]", cha.Id, cha.Name, cha.Class);
        // User.Instance.CurrentCharacter = cha;
        selectCharacterId = cha.Id;
        myCharacterView.SetChar((int)cha.Class);

        for (int i = 0; i < uiCharList.childCount; i++)
        {
            Transform transform = uiCharList.GetChild(i);
            var ci = transform.GetComponent<MyCharInfo>();
            if (ci.index == idx)
            {
                ci.avatar.gameObject.SetActive(true);
            }
            else
            {
                ci.avatar.gameObject.SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// UI 界面上点击返回按钮
    /// </summary>
    public void OnClickBack()
    {
        SceneManager.Instance.LoadScene("MyLoading");
    }

    /// <summary>
    /// UI 界面上点击进入游戏按钮
    /// </summary>
    public void OnClickEnterGame()
    {
        // 进入角色，只需要告诉服务端哪个角色ID进入了就行了
        // UserService.Instance.SendEnterGame(User.Instance.CurrentCharacter.Id);
        if (selectCharacterId >= 0)
        {
            UserService.Instance.SendEnterGame(selectCharacterId);
        }
    }
}
