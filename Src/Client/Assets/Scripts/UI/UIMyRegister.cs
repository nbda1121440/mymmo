﻿using Services;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMyRegister : MonoBehaviour {

    public Text email;
    public Text password;
    public Text confirmPassword;
    public Button cancelButton;

    void Start()
    {
        UserService.Instance.OnRegister = OnRegister;
    }

    private void OnRegister(Result result, string msg)
    {
        // MessageBox.Show("注册返回结果：result = " + result + " msg = " + msg);
        if (result == Result.Success)
        {
            var confirm = MessageBox.Show("注册成功", "注册结果", MessageBoxType.Information, "返回登录");
            confirm.OnYes = () =>
            {
                // 点击返回按钮
                cancelButton.onClick.Invoke();
            };
        }
        else
        {
            MessageBox.Show("注册失败，" + msg, "注册结果", MessageBoxType.Error);
        }
    }

    public void OnClickRegister()
    {
        if (string.IsNullOrEmpty(email.text))
        {
            MessageBox.Show("请输入用户名");
            return;
        }
        if (string.IsNullOrEmpty(password.text))
        {
            MessageBox.Show("请输入密码");
            return;
        }
        if (string.IsNullOrEmpty(confirmPassword.text))
        {
            MessageBox.Show("请输入确认密码");
            return;
        }
        if (!password.text.Equals(confirmPassword.text))
        {
            MessageBox.Show("两次密码不匹配");
            return;
        }
        UserService.Instance.SendRegister(email.text, password.text);
    }
}
