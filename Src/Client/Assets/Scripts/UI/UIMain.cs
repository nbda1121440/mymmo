﻿using Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMain : MonoSingleton<UIMain> {

    public Text name;
    public Text level;

    public UITeam TeamWindow;

    //void Start () {
    //       name.text = User.Instance.CurrentCharacter.Name + "(" + User.Instance.CurrentCharacter.Id + ")";
    //       level.text = User.Instance.CurrentCharacter.Level.ToString();
    //}

    protected override void OnStart()
    {
        UpdateAvatar();
    }

    //void Update () {

    //}

    void UpdateAvatar()
    {
        this.name.text = string.Format("{0}[{1}]", User.Instance.CurrentCharacter.Name, User.Instance.CurrentCharacter.Id);
        this.level.text = User.Instance.CurrentCharacter.Level.ToString();
    }

    //public void BackToCharSelect()
    //{
    //    Debug.LogFormat("BackToCharSelect");
    //    SceneManager.Instance.LoadScene("MyCharSelect");
    //    Services.UserService.Instance.SendGameLeave();
    //}

    public void ChangeMap()
    {
        Debug.LogFormat("ChangeMap");
        SceneManager.Instance.LoadScene("Map01");
    }

    public void OnClickTest()
    {
        UITest test = UIManager.Instance.Show<UITest>();
        test.SetTitle("这是一个测试UI");
        test.OnClose += Test_OnClose;
    }

    private void Test_OnClose(UIWindow sender, UIWindow.WindowResult result)
    {
        MessageBox.Show("点击了对话框：" + result, "对话框响应结果", MessageBoxType.Information);
    }

    public void OnClickBag()
    {
        UIManager.Instance.Show<UIBag>();
    }

    public void OnClickCharEquip()
    {
        UIManager.Instance.Show<UICharEquip>();
    }

    public void OnClickQuest()
    {
        UIManager.Instance.Show<UIQuestSystem>();
    }

    public void OnClickFriend()
    {
        UIManager.Instance.Show<UIFriends>();
    }

    public void OnClickGuild()
    {
        GuildManager.Instance.ShowGuild();
    }

    public void OnClickRide()
    { }

    public void OnClickSetting()
    {
        UIManager.Instance.Show<UISetting>();
    }

    public void OnClickSkill()
    { }

    public void ShowTeamUI(bool show)
    {
        TeamWindow.ShowTeam(show);
    }
}
