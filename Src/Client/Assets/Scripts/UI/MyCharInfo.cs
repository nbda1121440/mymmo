﻿using SkillBridge.Message;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyCharInfo : MonoBehaviour {

    private NCharacterInfo charInfo;
    public Text level;
    public Text name;
    public Image avatar;
    public int index;

    public NCharacterInfo CharInfo {
        get
        {
            return charInfo;
        }
        set
        {
            this.charInfo = value;
            // 给等级、名称赋值
            level.text = this.charInfo.Level + "级";
            name.text = this.charInfo.Name;
            avatar.gameObject.SetActive(false);
        }
    }

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
