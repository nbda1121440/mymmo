﻿using Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMinimap : MonoSingleton<UIMinimap>
{
    public Collider minimapBoundingBox;
    public Image minimap;
    public Image arrow;
    public Text mapName;
    private Transform playerTransform;

    //   // Use this for initialization
    //   protected override void OnStart () {
    //       MinimapManager.Instance.minimap = this;
    //       this.UpdateMap();
    //}

    // 现在新的 MonoSingleton 把 OnStart 放在 Awake 里面实现，小地图需要在某个东西后面 Start 所以不放在 Awake 里面实现
    public void Start()
    {
        Debug.LogFormat("UIMinimap Start " + this.GetInstanceID());
        MinimapManager.Instance.minimap = this;
        this.UpdateMap();
    }

    public void UpdateMap()
    {
        this.mapName.text = User.Instance.CurrentMapData.Name;
        //if (this.minimap.overrideSprite == null)
        //{
        //    // overrideSprite 是运行时动态修改的图片
        //    // sprite 是运行前静态的图片
        //    this.minimap.overrideSprite = MinimapManager.Instance.LoadCurrentMinimap();
        //}
        this.minimap.overrideSprite = MinimapManager.Instance.LoadCurrentMinimap();

        this.minimap.SetNativeSize();
        this.minimap.transform.localPosition = Vector3.zero;
        this.minimapBoundingBox = MinimapManager.Instance.MinimapBoundingBox;
        this.playerTransform = null;

        //// 首先把玩家当前的位置缓存下来
        //while (User.Instance.CurrentCharacterObject == null)
        //{
        //    // 说明 GameObjectManager 还没有把玩家角色创建出来，所以等待一帧
        //    yield return null;
        //}
        //// 说明 GameObjectManager 已经把玩家角色创建出来而来，此时才能进行赋值
        //this.playerTransform = User.Instance.CurrentCharacterObject.transform;
    }
	
	void Update () {

        if (playerTransform == null)
        {
            playerTransform = MinimapManager.Instance.PlayerTransform;
        }
        if (this.minimapBoundingBox == null || this.playerTransform == null)
        {
            // 此时 GameObjectManager 还没有把玩家角色创建出来
            // 等玩家角色创建出来之后才能进行后续的小地图偏移操作
            return;
        }

        // 每帧更新的时候，算出玩家在 minimapBoundingBox 中的位置
        float realWidth = minimapBoundingBox.bounds.size.x;
        float realHeight = minimapBoundingBox.bounds.size.z;

        float relaX = playerTransform.position.x - minimapBoundingBox.bounds.min.x;
        float relaY = playerTransform.position.z - minimapBoundingBox.bounds.min.z;

        // 我们去移动 minimap.sprite 的坐标实现小地图移动会比较麻烦，这里推荐修改小地图的 pivot 来模拟小地图移动
        float pivotX = relaX / realWidth;
        float pivotY = relaY / realHeight;

        this.minimap.rectTransform.pivot = new Vector2(pivotX, pivotY);
        this.minimap.rectTransform.localPosition = Vector2.zero;
        this.arrow.transform.eulerAngles = new Vector3(0, 0, playerTransform.eulerAngles.y * -1);
	}
}
