﻿using Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class UINameBar : MonoBehaviour {

    // public Text avaverName;

    public Image avatar;
    public Text characterName;

    public Character character;

    public Camera camera;


    // Use this for initialization
    void Start () {
		if(this.character!=null)
        {
            if (character.info.Type == SkillBridge.Message.CharacterType.Monster)
            {
                this.avatar.gameObject.SetActive(false);
            }
            else
            {
                this.avatar.gameObject.SetActive(true);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        this.UpdateInfo();

        this.transform.forward = camera.transform.forward;
        
	}

    void UpdateInfo()
    {
        if (this.character != null)
        {
            string name = this.character.Name + " Lv." + this.character.info.Level;
            //if(name != this.avaverName.text)
            //{
            //    this.avaverName.text = name;
            //}
            if (name != this.characterName.text)
            {
                this.characterName.text = name;
            }
        }
    }
}
