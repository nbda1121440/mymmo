﻿using Services;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMyLogin : MonoBehaviour {

    public Text username;
    public Text password;

	// Use this for initialization
	void Start () {
        UserService.Instance.OnLogin = OnLogin;
	}

    // Update is called once per frame
    void Update () {
		
	}

    public void OnClickLogin()
    {
        if (string.IsNullOrEmpty(username.text))
        {
            MessageBox.Show("用户名不能为空");
            return;
        }
        if (string.IsNullOrEmpty(password.text))
        {
            MessageBox.Show("密码不能为空");
            return;
        }
        UserService.Instance.SendLogin(username.text, password.text);
    }

    private void OnLogin(Result result, string msg)
    {
        // MessageBox.Show("登录成功，result = " + result + ", msg = " + msg);
        if (result == Result.Success)
        {
            // 进入到角色选择场景
            SceneManager.Instance.LoadScene("MyCharSelect");
        }
        else
        {
            MessageBox.Show("登录失败，msg = " + msg);
        }
    }
}
