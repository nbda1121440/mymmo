﻿using Common.Data;
using Models;
using SkillBridge.Message;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShop : UIWindow {

    public Text title;
    public Text money;

    public GameObject shopItem;
    ShopDefine shop;
    public Transform[] itemRoot;

    void Start()
    {
        StartCoroutine(InitItems());

        // 监听金币变化
        StatusService.Instance.RegisterStatusNotify(StatusType.Money, OnMoneyNotify);
        this.OnClose += UIShopOnClose;
    }

    private void UIShopOnClose(UIWindow sender, UIWindow.WindowResult result)
    {
        // MessageBox.Show("点击了对话框：" + result, "对话框响应结果", MessageBoxType.Information);
        // 取消监听金币变化
        StatusService.Instance.UnregisterStatusNotify(StatusType.Money, OnMoneyNotify);
    }

    bool OnMoneyNotify(NStatus status)
    {
        int currentMoney = int.Parse(money.text);

        if (status.Action == StatusAction.Add)
        {
            currentMoney += status.Value;
        }
        if (status.Action == StatusAction.Delete)
        {
            currentMoney -= status.Value;
        }
        money.text = currentMoney.ToString();
        return true;
    }

    IEnumerator InitItems()
    {
        int count = 0;
        int page = 0;
        foreach (var kv in DataManager.Instance.ShopItems[shop.ID])
        {
            if (kv.Value.Status > 0)
            {
                GameObject go = Instantiate(shopItem, itemRoot[page]);
                UIShopItem ui = go.GetComponent<UIShopItem>();
                ui.SetShopItem(kv.Key, kv.Value, this);
                count++;
                if (count >= 10)
                {
                    count = 0;
                    page++;
                    itemRoot[page].gameObject.SetActive(true);
                }
            }
        }
        yield return null;
    }

    public void SetShop(ShopDefine shop)
    {
        this.shop = shop;
        this.title.text = shop.Name;
        this.money.text = User.Instance.CurrentCharacter.Gold.ToString();
    }

    private UIShopItem selectedItem;
    public void SelectShopItem(UIShopItem item)
    {
        if (selectedItem != null)
        {
            selectedItem.Selected = false;
        }
        selectedItem = item;
    }

    public void OnClickBuy()
    {
        if (this.selectedItem == null)
        {
            MessageBox.Show("请选择要购买的道具", "购买提示");
            return;
        }
        if (!ShopManager.Instance.BuyItem(this.shop.ID, this.selectedItem.ShopItemID))
        {

        }
    }
}
