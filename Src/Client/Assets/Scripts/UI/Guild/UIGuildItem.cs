﻿using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIGuildItem : ListView.ListViewItem
{
    public NGuildInfo Info;

    public Text id;
    public Text name;
    public Text memberCount;
    public Text leader;

    public Image background;
    public Sprite normalBg;
    public Sprite selectedBg;

    public override void onSelected(bool selected)
    {
        this.background.overrideSprite = selected ? selectedBg : normalBg;
    }

    public void SetGuildInfo(NGuildInfo item)
    {
        this.Info = item;
        if (this.id != null)
        {
            this.id.text = this.Info.Id.ToString();
        }
        if (this.name != null)
        {
            this.name.text = this.Info.GuildName;
        }
        if (this.memberCount != null)
        {
            this.memberCount.text = this.Info.memberCount.ToString();
        }
        if (this.leader != null)
        {
            this.leader.text = this.Info.leaderName;
        }
    }
}
