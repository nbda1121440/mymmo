﻿using Common.Data;
using Services;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMyCharCreate : MonoBehaviour {

    public MyCharacterView myCharacterView;
    public GameObject[] titles;
    public Text description;
    public Text[] classDescriptions;
    public Button backButton;
    public UIMyCharSelect uiMyCharSelect;

    // 新建角色的名称
    public Text nickname;
    // 新建角色的职业
    public int classIndex = 0;

    public IEnumerator Start()
    {
        // 因为客户端是不能卡住的，所以需要通过协程来加载数据
        yield return DataManager.Instance.LoadData();

        // 一开始默认选择要创建战士角色
        ShowCharacter(1);

        // 将战士、法师、弓箭手按钮下面的文本替换一下
        for (int i = 1; i <= 3; i++)
        {
            classDescriptions[i].text = DataManager.Instance.Characters[i].Name;
        }

        // 监听一下创建用户的事件
        UserService.Instance.OnCreateCharacter = OnCreateCharacter;
    }

    private void OnCreateCharacter(Result result, string msg)
    {
        // var messagebox = MessageBox.Show("创建角色成功，result = " + result + ", msg = " + msg);
        var messagebox = MessageBox.Show("创建角色成功", "创建角色", MessageBoxType.Information, "返回角色选择");
        messagebox.OnYes = () =>
        {
            // 更新一下角色列表
            UserService.Instance.SendGetInfo();

            // 再返回到角色选择界面
            if (backButton != null)
            {
                backButton.onClick.Invoke();

                // 刷新一下角色选择列表
                uiMyCharSelect.RenewCharacterList();
            }
        };
    }

    private void ShowCharacter(int classIndex)
    {
        // 显示对应的标题
        for (int i = 1; i <= 3; i++)
        {
            titles[i].SetActive(i == classIndex);
        }
        titles[classIndex].SetActive(true);
        // 显示对应的职业描述
        CharacterDefine characterDefine = DataManager.Instance.Characters[classIndex];
        description.text = characterDefine.Description;
        // 先改MyCharacterView 里面的角色
        myCharacterView.SetChar(classIndex);
        // 修改职业
        this.classIndex = classIndex;
    }

    /// <summary>
    /// 点击战士、法师、弓箭手按钮的时候，修改 MyCharcterView 里面的角色，并且修改标题和描述
    /// index: 0战士、1法师、2弓箭手
    /// </summary>
	public void OnClickChangeChar(int index)
    {
        ShowCharacter(index);
    }

    /// <summary>
    /// 点击发送创建角色
    /// </summary>
    public void OnClickSendCreateCharacter()
    {
        if (string.IsNullOrEmpty(nickname.text))
        {
            MessageBox.Show("昵称不能为空");
            return;
        }
        UserService.Instance.SendCreateCharacter(nickname.text, classIndex);
    }
}
