﻿using Common.Data;
using Managers;
using Models;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Services
{
    class MapService : Singleton<MapService>, IDisposable
    {
        public MapService()
        {
            MessageDistributer.Instance.Subscribe<SkillBridge.Message.MapCharacterEnterResponse>(this.OnMapCharacterEnter);
            MessageDistributer.Instance.Subscribe<SkillBridge.Message.MapCharacterLeaveResponse>(this.OnMapCharacterLeave);
            MessageDistributer.Instance.Subscribe<MapEntitySyncResponse>(this.OnMapEntitySync);
        }

        public void Dispose()
        {
            MessageDistributer.Instance.Unsubscribe<SkillBridge.Message.MapCharacterEnterResponse>(this.OnMapCharacterEnter);
            MessageDistributer.Instance.Unsubscribe<SkillBridge.Message.MapCharacterLeaveResponse>(this.OnMapCharacterLeave);
            MessageDistributer.Instance.Unsubscribe<MapEntitySyncResponse>(this.OnMapEntitySync);
        }

        public void Init()
        { }

        public int CurrentMapId { get; set; }

        void OnMapCharacterEnter(object sender, MapCharacterEnterResponse response)
        {
            Debug.LogFormat("OnMapCharacterEnter MapId:{0} Count:{1}", response.mapId, response.Characters.Count);

            // 按照下面的写法，可能会出现切换场景后，角色不见的问题，因此我改成了切换场景完毕之后再添加角色
            //foreach (var cha in response.Characters)
            //{
            //    if (User.Instance.CurrentCharacter == null || User.Instance.CurrentCharacter.Id == cha.Id)
            //    {
            //        // 当前角色切换地图
            //        // 这里再赋一次值是在登录期间，服务器可能给这个角色发了一些经验，所以再同步一次该角色的数据会更好一点
            //        User.Instance.CurrentCharacter = cha;
            //    }
            //    CharacterManager.Instance.AddCharacter(cha);
            //}
            //Debug.LogFormat("CurrentMapId={0} response.mapId={1}", CurrentMapId, response.mapId);
            //if (CurrentMapId != response.mapId)
            //{
            //    // 说明当前角色切换了地图
            //    this.EnterMap(response.mapId);
            //    this.CurrentMapId = response.mapId;
            //}

            // 当前是哪个角色还是要记录一下，否则后面可能会报空指针异常
            foreach (var cha in response.Characters)
            {
                if (User.Instance.CurrentCharacter == null || (cha.Type == CharacterType.Player && User.Instance.CurrentCharacter.Id == cha.Id))
                {
                    // 这个就是我当前正在登录的角色
                    User.Instance.CurrentCharacter = cha;
                    break;
                }
            }

            Debug.LogFormat("CurrentMapId={0} response.mapId={1}", CurrentMapId, response.mapId);
            if (CurrentMapId == response.mapId)
            {
                // 地图没有发生变化，所以可以把新角色直接加进来
                foreach (var cha in response.Characters)
                {
                    // 往地图上添加角色
                    CharacterManager.Instance.AddCharacter(cha);
                }
            }
            else
            {
                // 说明切换了地图，等完全切换好地图之后再添加角色
                this.EnterMap(response.mapId, response.Characters);
                this.CurrentMapId = response.mapId;
            }
        }

        internal void SendMapTeleport(int teleporterID)
        {
            Debug.LogFormat("Send MapTeleportRequest teleporterID:{0}", teleporterID);
            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.mapTeleport = new MapTeleportRequest();
            message.Request.mapTeleport.teleporterId = teleporterID;
            NetClient.Instance.SendMessage(message);
        }

        private void OnMapCharacterLeave(object sender, MapCharacterLeaveResponse response)
        {
            Debug.LogFormat("OnMapCharacterLeave: CharID:{0}", response.entityId);
            if (response.entityId != User.Instance.CurrentCharacter.EntityId)
            {
                // 当前离开地图的不是自己，就将它从角色管理器中删除
                CharacterManager.Instance.RemoveCharacter(response.entityId);
            }
            else
            {
                // 当前离开地图的是自己，要把所有角色从角色管理器中删除
                CharacterManager.Instance.Clear();
            }
        }

        private void EnterMap(int mapId)
        {
            if (DataManager.Instance.Maps.ContainsKey(mapId))
            {
                MapDefine map = DataManager.Instance.Maps[mapId];
                User.Instance.CurrentMapData = map;
                SceneManager.Instance.LoadScene(map.Resource);
            }
            else
            {
                Debug.LogErrorFormat("EnterMap: Map {0} not existed", mapId);
            }
        }

        /**
         * 进入地图，同时把角色添加到地图中
         */
        private void EnterMap(int mapId, List<NCharacterInfo> nCharacters)
        {
            if (DataManager.Instance.Maps.ContainsKey(mapId))
            {
                MapDefine map = DataManager.Instance.Maps[mapId];
                User.Instance.CurrentMapData = map;
                SceneManager.Instance.LoadScene(map.Resource, nCharacters);
            }
            else
            {
                Debug.LogErrorFormat("EnterMap: Map {0} not existed", mapId);
            }
        }

        public void SendMapEntitySync(EntityEvent entityEvent, NEntity entity)
        {
        	// todo 后续可能要打开这个日志
            // Debug.LogFormat("MapEntityUpdateRequest: ID:{0} POS:{1} DIR:{2} SPD:{3}", entity.Id, entity.Position.String(), entity.Direction.String(), entity.Speed);
            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.mapEntitySync = new MapEntitySyncRequest();
            message.Request.mapEntitySync.entitySync = new NEntitySync()
            {
                Id = entity.Id,
                Event = entityEvent,
                Entity = entity
            };
            NetClient.Instance.SendMessage(message);
        }

        private void OnMapEntitySync(object sender, MapEntitySyncResponse response)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendFormat("MapEntityUpdateResponse: Entitys:{0}", response.entitySyncs.Count);
            sb.AppendLine();
            foreach (var entity in response.entitySyncs)
            {
                Managers.EntityManager.Instance.OnEntitySync(entity);
                sb.AppendFormat("   [{0}]evt:{1} entity:{2}", entity.Id, entity.Event, entity.Entity.String());
                sb.AppendLine();
            }
            Debug.Log(sb.ToString());
        }
    }
}
