﻿using Common.Data;
using Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterObject : MonoBehaviour {

    public int ID;
    Mesh mesh = null;

	// Use this for initialization
	void Start () {
        this.mesh = this.GetComponent<MeshFilter>().sharedMesh;
        Debug.Log("TeleporterObject Start ID = " + ID);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

#if UNITY_EDITOR
    // 仅在 Unity 编辑器下绘制
    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        if (this.mesh != null)
        {
            // 可以自己试一下，看看这个 0.5f 是什么意思，其实就是为了让这个东西看起来和实际的一致
            Gizmos.DrawWireMesh(this.mesh,
                // this.transform.position + Vector3.up * this.transform.localScale.y * 0.5f,
                this.transform.position,
                this.transform.rotation,
                this.transform.localScale);
        }

        // 我们从传送点传送的时候，希望知道有一个默认的方向
        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.ArrowHandleCap(0,
            this.transform.position,
            this.transform.rotation,
            1f,
            EventType.Repaint);
    }
#endif

    void OnTriggerEnter(Collider other)
    {
        PlayerInputController playerController = other.GetComponent<PlayerInputController>();
        if (playerController != null && playerController.isActiveAndEnabled)
        {
            TeleporterDefine td = DataManager.Instance.Teleporters[this.ID];
            if (td == null)
            {
                Debug.LogErrorFormat("TeleporterObject: Character [{0}] Enter Teleporter [{1}], But TeleporterDefine not existed", playerController.character.info.Name, this.ID);
                return;
            }
            Debug.LogFormat("TeleporterObject: Character [{0}] Enter Teleporter [{1}]:[{2}]", playerController.character.info.Name, td.ID, td.Name);
            if (td.LinkTo > 0)
            {
                if (DataManager.Instance.Teleporters.ContainsKey(td.LinkTo))
                {
                    MapService.Instance.SendMapTeleport(this.ID);
                }
                else
                {
                    Debug.LogErrorFormat("Teleporter ID:{0} LinkID {1} error!", td.ID, td.LinkTo);
                }
            }
        }
    }
}
