﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCharacterView : MonoBehaviour {

    // 各个职业的角色
    public GameObject[] chars;

    // 角色脚底的阴影
    public GameObject quad;

    /// <summary>
    /// index 从1开始，分别是战士、法师、弓箭手
    /// </summary>
    /// <param name="index"></param>
    public void SetChar(int index)
    {
        bool isAllNotActive = true;
        for (int i = 1; i <= 3; i++)
        {
            chars[i].SetActive(i == index);
            if (i == index)
            {
                isAllNotActive = false;
            }
        }
        if (isAllNotActive)
        {
            quad.SetActive(false);
        }
        else
        {
            quad.SetActive(true);
        }
    }
}
