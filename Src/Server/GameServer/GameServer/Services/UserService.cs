﻿using Common;
using GameServer.Entities;
using GameServer.Managers;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Services
{
    class UserService : Singleton<UserService>
    {
        public void Init()
        { }

        public void Start()
        {
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<UserRegisterRequest>(this.OnUserRegisterRequest);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<UserLoginRequest>(this.OnUserLoginRequest);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<UserCreateCharacterRequest>(this.OnUserCreateCharacterRequest);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<UserGameEnterRequest>(this.OnUserGameEnterRequest);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<UserGameLeaveRequest>(this.OnUserGameLeaveRequest);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<UserGetInfoRequest>(this.OnUserGetInfoRequest);
        }

        void OnUserLoginRequest(NetConnection<NetSession> conn, UserLoginRequest request)
        {
            Log.InfoFormat("OnUserLoginRequest: User:{0}, Password:{1}", request.User, request.Passward);

            //NetMessage message = new NetMessage();
            //message.Response = new NetMessageResponse();
            //message.Response.userLogin = new UserLoginResponse();

            conn.Session.Response.userLogin = new UserLoginResponse();

            TUser user = DBService.Instance.Entities.Users.Where(u => u.Username.Equals(request.User)).FirstOrDefault();
            if (user == null)
            {
                conn.Session.Response.userLogin.Result = Result.Failed;
                conn.Session.Response.userLogin.Errormsg = "用户不存在";
            }
            else if (user.Password != request.Passward)
            {
                conn.Session.Response.userLogin.Result = Result.Failed;
                conn.Session.Response.userLogin.Errormsg = "密码错误";
            }
            else
            {
                conn.Session.User = user;

                conn.Session.Response.userLogin.Result = Result.Success;
                conn.Session.Response.userLogin.Errormsg = "None";
                conn.Session.Response.userLogin.Userinfo = new NUserInfo();
                conn.Session.Response.userLogin.Userinfo.Id = (int)user.ID;
                conn.Session.Response.userLogin.Userinfo.Player = new NPlayerInfo();
                conn.Session.Response.userLogin.Userinfo.Player.Id = user.Player.ID;

                foreach (var c in user.Player.Characters)
                {
                    NCharacterInfo info = new NCharacterInfo();
                    info.Id = c.ID;
                    info.Name = c.Name;
                    info.Type = CharacterType.Player;
                    info.Class = (CharacterClass)c.Class;
                    info.ConfigId = c.ID;
                    conn.Session.Response.userLogin.Userinfo.Player.Characters.Add(info);
                }
            }
            conn.SendResponse();
            //if (user == null)
            //{
            //    // 说明数据库中没有这个用户
            //    message.Response.userLogin.Result = Result.Failed;
            //    message.Response.userLogin.Errormsg = "用户不存在";
            //}
            //else
            //{
            //    // 判断一下密码是否正确
            //    if (user.Password.Equals(request.Passward))
            //    {
            //        // 说明密码正确，登录成功
            //        message.Response.userLogin.Result = Result.Success;
            //        message.Response.userLogin.Errormsg = "None";

            //        // 将用户下面所有的角色返回回去
            //        message.Response.userLogin.Userinfo = new NUserInfo();
            //        message.Response.userLogin.Userinfo.Id = 1;
            //        message.Response.userLogin.Userinfo.Player = new NPlayerInfo();
            //        message.Response.userLogin.Userinfo.Player.Id = user.Player.ID;
            //        foreach (var c in user.Player.Characters)
            //        {
            //            NCharacterInfo info = new NCharacterInfo();
            //            info.Id = c.ID;
            //            info.Name = c.Name;
            //            info.Type = CharacterType.Player;
            //            info.Class = (CharacterClass)c.Class;
            //            info.Tid = c.ID;
            //            message.Response.userLogin.Userinfo.Player.Characters.Add(info);
            //        }
            //    }
            //    else
            //    {
            //        // 说明密码错误，登录失败
            //        message.Response.userLogin.Result = Result.Failed;
            //        message.Response.userLogin.Errormsg = "密码错误";
            //    }

            //    // 登录成功之后，需要往 Session 中存储用户信息
            //    sender.Session.User = user;

            //    byte[] data = PackageHandler.PackMessage(message);
            //    sender.SendData(data, 0, data.Length);
            //}
        }

        void OnUserRegisterRequest(NetConnection<NetSession> conn, UserRegisterRequest request)
        {
            Log.InfoFormat("OnUserRegisterRequest: User:{0}, Password:{1}", request.User, request.Passward);

            conn.Session.Response.userRegister = new UserRegisterResponse();

            // 首先看看用户是否存在
            TUser user = DBService.Instance.Entities.Users.Where(u => u.Username.Equals(request.User)).FirstOrDefault();
            if (user != null)
            {
                // 说明数据库中之前已经有了这个用户，那么不允许进行注册
                conn.Session.Response.userRegister.Result = Result.Failed;
                conn.Session.Response.userRegister.Errormsg = "用户已存在";
            }
            else
            {
                // 说明数据库中之前没有这个用户，那么就往数据库中添加 Player 和 User
                TPlayer player = DBService.Instance.Entities.Players.Add(new TPlayer());
                DBService.Instance.Entities.Users.Add(new TUser() { Username = request.User, Password = request.Passward, Player = player });
                DBService.Instance.Entities.SaveChanges();
                conn.Session.Response.userRegister.Result = Result.Success;
                conn.Session.Response.userRegister.Errormsg = "None";
            }

            conn.SendResponse();

            //NetMessage message = new NetMessage();
            //message.Response = new NetMessageResponse();
            //message.Response.userRegister = new UserRegisterResponse();

            //TUser user = DBService.Instance.Entities.Users.Where(u => u.Username.Equals(request.User)).FirstOrDefault();
            //if (user != null)
            //{
            //    // 说明数据库中之前已经有了这个用户，那么不允许进行注册
            //    message.Response.userRegister.Result = Result.Failed;
            //    message.Response.userRegister.Errormsg = "用户已存在";
            //}
            //else
            //{
            //    // 说明数据库中之前没有这个用户，就往数据库中添加 Player 和 User
            //    TPlayer player = DBService.Instance.Entities.Players.Add(new TPlayer());
            //    DBService.Instance.Entities.Users.Add(new TUser() { Username = request.User, Password = request.Passward, Player = player });
            //    DBService.Instance.Entities.SaveChanges();
            //    message.Response.userRegister.Result = Result.Success;
            //    message.Response.userRegister.Errormsg = "None";
            //}

            //byte[] data = PackageHandler.PackMessage(message);
            //sender.SendData(data, 0, data.Length);
        }

        void OnUserCreateCharacterRequest(NetConnection<NetSession> conn, UserCreateCharacterRequest request)
        {
            Log.InfoFormat("OnUserCreateCharacterRequest: nickname:{0}, classIndex:{1}", request.Name, request.Class);

            TCharacter character = new TCharacter()
            {
                Name = request.Name,
                Class = (int) request.Class,
                TID = (int) request.Class,
                Level = 1,
                MapID = 1,
                MapPosX = 5000,     // 初始出生位置 X
                MapPosY = 4000,     // 初始出生位置 Y
                MapPosZ = 820,
                Gold = 100000,      // 初始 10 万金币
                Equips = new byte[28]
            };
            //character.TID = (int)request.Class;
            //character.Class = (int)request.Class;
            //character.Name = request.Name;
            //character.Player = sender.Session.User.Player;
            //character.MapID = 1;
            //character.MapPosX = 5000;
            //character.MapPosY = 4000;
            //character.MapPosZ = 820;


            var bag = new TCharacterBag();
            bag.Owner = character;
            bag.Items = new byte[0];
            bag.Unlocked = 20;
            TCharacterItem it = new TCharacterItem();
            character.Bag = DBService.Instance.Entities.TCharacterBags.Add(bag);

            character.Items.Add(new TCharacterItem()
            {
                Owner = character,
                ItemID = 1,
                ItemCount = 20
            });
            character.Items.Add(new TCharacterItem() {
                Owner = character,
                ItemID = 2,
                ItemCount = 20
            });

            character = DBService.Instance.Entities.Characters.Add(character);
            conn.Session.User.Player.Characters.Add(character);
            DBService.Instance.Entities.SaveChanges();

            conn.Session.Response.createChar = new UserCreateCharacterResponse();
            conn.Session.Response.createChar.Result = Result.Success;
            conn.Session.Response.createChar.Errormsg = "None";

            // 这里在创建角色之后，将该用户下面所有的角色都返回回去
            foreach (var c in conn.Session.User.Player.Characters)
            {
                NCharacterInfo info = new NCharacterInfo();
                info.Id = c.ID;
                info.Name = c.Name;
                info.Type = CharacterType.Player;
                info.Class = (CharacterClass)c.Class;
                info.ConfigId = c.TID;
                conn.Session.Response.createChar.Characters.Add(info);
            }

            conn.SendResponse();

            //NetMessage message = new NetMessage();
            //message.Response = new NetMessageResponse();
            //message.Response.createChar = new UserCreateCharacterResponse();
            //message.Response.createChar.Result = Result.Success;
            //message.Response.createChar.Errormsg = "None";

            //// 这里在创建角色之后，将该用户下面所有的角色都返回回去
            //foreach (var c in sender.Session.User.Player.Characters)
            //{
            //    NCharacterInfo info = new NCharacterInfo();
            //    info.Id = 0;
            //    info.Name = c.Name;
            //    info.Type = CharacterType.Player;
            //    info.Class = (CharacterClass)c.Class;
            //    info.Tid = c.ID;
            //    message.Response.createChar.Characters.Add(info);
            //}

            //byte[] data = PackageHandler.PackMessage(message);
            //sender.SendData(data, 0, data.Length);
        }

        void OnUserGameEnterRequest(NetConnection<NetSession> conn, UserGameEnterRequest request)
        {
            Log.InfoFormat("OnUserGameEnterRequest characterId:{0}", request.characterIdx);

            // 查出进入的角色
            TCharacter enteredCharacter = null;
            for (int i = 0; i < conn.Session.User.Player.Characters.Count; i++)
            {
                TCharacter tCharacter = conn.Session.User.Player.Characters.ElementAt(i);
                if (tCharacter.ID == request.characterIdx)
                {
                    // 这个就是进入的角色
                    enteredCharacter = tCharacter;
                    break;
                }
            }

            // 要将这个进入的角色添加到角色管理器中
            Character character = CharacterManager.Instance.AddCharacter(enteredCharacter);

            SessionManager.Instance.AddSession(character.Id, conn);

            conn.Session.Response.gameEnter = new UserGameEnterResponse();
            conn.Session.Response.gameEnter.Result = Result.Success;
            conn.Session.Response.gameEnter.Errormsg = "None";

            // 进入成功，发送初始角色信息
            conn.Session.Character = character;
            conn.Session.PostResponser = character;

            conn.Session.Response.gameEnter.Character = character.Info;
            conn.SendResponse();

            //// 构造游戏进入的返回消息
            //NetMessage message = new NetMessage();
            //message.Response = new NetMessageResponse();
            //message.Response.gameEnter = new UserGameEnterResponse();
            //message.Response.gameEnter.Result = Result.Success;
            //message.Response.gameEnter.Errormsg = "None";
            //message.Response.gameEnter.Character = character.Info;

            //byte[] data = PackageHandler.PackMessage(message);
            //sender.SendData(data, 0, data.Length);

            // 通知地图管理器，角色进入了地图

            MapManager.Instance[enteredCharacter.MapID].CharacterEnter(conn, character);
        }

        void OnUserGameLeaveRequest(NetConnection<NetSession> conn, UserGameLeaveRequest request)
        {
            Character character = conn.Session.Character;
            Log.InfoFormat("OnUserGameLeaveRequest: characterID:{0}:{1} Map:{2}", character.Id, character.Info.Name, character.Info.mapId);

            CharacterLeave(character);

            conn.Session.Response.gameLeave = new UserGameLeaveResponse();
            conn.Session.Response.gameLeave.Result = Result.Success;
            conn.Session.Response.gameLeave.Errormsg = "None";
            conn.SendResponse();

            //NetMessage message = new NetMessage();
            //message.Response = new NetMessageResponse();
            //message.Response.gameLeave = new UserGameLeaveResponse();
            //message.Response.gameLeave.Result = Result.Success;
            //message.Response.gameLeave.Errormsg = "None";

            //byte[] data = PackageHandler.PackMessage(message);
            //sender.SendData(data, 0, data.Length);
        }

        public void CharacterLeave(Character character)
        {
            Log.InfoFormat("CharacterLeave: characterID:{0}:{1}", character.Id, character.Info.Name);
            SessionManager.Instance.RemoveSession(character.Id);
            CharacterManager.Instance.RemoveCharacter(character.Id);
            character.Clear();
            MapManager.Instance[character.Info.mapId].CharacterLeave(character);
        }

        //internal void CharacterLeave(Character character)
        //{
        //    CharacterManager.Instance.RemoveCharacter(character.Id);
        //    MapManager.Instance[character.Info.mapId].CharacterLeave(character);
        //    character.Clear();
        //}

        private void OnUserGetInfoRequest(NetConnection<NetSession> conn, UserGetInfoRequest request)
        {
            var username = conn.Session.User.Username;

            TUser user = DBService.Instance.Entities.Users.Where(u => u.Username.Equals(username)).FirstOrDefault();
            if (user == null)
            {
                conn.Session.Response.userLogin.Result = Result.Failed;
                conn.Session.Response.userLogin.Errormsg = "用户不存在";
            }
            else
            {
                conn.Session.User = user;

                conn.Session.Response.userLogin.Result = Result.Success;
                conn.Session.Response.userLogin.Errormsg = "None";
                conn.Session.Response.userLogin.Userinfo = new NUserInfo();
                conn.Session.Response.userLogin.Userinfo.Id = (int)user.ID;
                conn.Session.Response.userLogin.Userinfo.Player = new NPlayerInfo();
                conn.Session.Response.userLogin.Userinfo.Player.Id = user.Player.ID;

                foreach (var c in user.Player.Characters)
                {
                    NCharacterInfo info = new NCharacterInfo();
                    info.Id = c.ID;
                    info.Name = c.Name;
                    info.Type = CharacterType.Player;
                    info.Class = (CharacterClass)c.Class;
                    info.ConfigId = c.ID;
                    conn.Session.Response.userLogin.Userinfo.Player.Characters.Add(info);
                }
            }
            conn.SendResponse();
        }
    }
}
