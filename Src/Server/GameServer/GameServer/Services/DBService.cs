﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

namespace GameServer.Services
{
    class DBService : Singleton<DBService>
    {
        ExtremeWorldEntities entities;

        public ExtremeWorldEntities Entities
        {
            get { return this.entities; }
        }

        public void Init()
        {
            entities = new ExtremeWorldEntities();
        }

        public void Save(bool async = false)
        {
            if (async)
            {
                // Save 的地方可以进行延迟 Save，这样保证每秒数据库的压力不会太大
                // 大部分游戏不会立即存储，因为性能问题
                entities.SaveChangesAsync();
            }
            else
            {
                entities.SaveChanges();
            }
        }
    }
}
