﻿using Common;
using GameServer.Entities;
using GameServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
    class TeamManager : Singleton<TeamManager>
    {
        public List<Team> teams = new List<Team>();
        public Dictionary<int, Team> characterTeams = new Dictionary<int, Team>();

        public void Init()
        { }

        public Team GetTeamByCharacter(int characterId)
        {
            Team team = null;
            this.characterTeams.TryGetValue(characterId, out team);
            return team;
        }

        public void AddTeamMember(Character leader, Character member)
        {
            if (leader.team == null)
            {
                leader.team = CreateTeam(leader);
            }
            leader.team.AddMember(member);
        }

        Team CreateTeam(Character leader)
        {
            Team team = null;
            for (int i = 0; i < this.teams.Count; i++)
            {
                team = this.teams[i];
                if (team.Members.Count == 0)
                {
                    team.AddMember(leader);
                    return team;
                }
            }
            team = new Team(leader);
            this.teams.Add(team);
            team.Id = this.teams.Count;
            return team;
        }
    }
}
