﻿using GameServer.Entities;
using GameServer.Models;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
    class MonsterManager
    {
        private Map map;

        public Dictionary<int, Monster> monsters = new Dictionary<int, Monster>();

        public void Init(Map map)
        {
            this.map = map;
        }

        public Monster Create(int spawnMonID, int spawnLevel, NVector3 position, NVector3 direction)
        {
            // 创建一个怪物
            Monster monster = new Monster(spawnMonID, spawnLevel, position, direction);

            // 将它放到实体管理器中
            EntityManager.Instance.AddEntity(this.map.ID, monster);

            // 设置怪物的 ID 和地图 ID
            monster.Info.Id = monster.entityId;
            monster.Info.EntityId = monster.entityId;
            monster.Info.mapId = this.map.ID;

            // 放到字典中
            monsters[monster.Id] = monster;

            // 让怪物进入地图
            this.map.MonsterEnter(monster);
            return monster;
        }
    }
}
