﻿using Common;
using GameServer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
    class CharacterManager : Singleton<CharacterManager>
    {
        public Dictionary<int, Character> Characters = new Dictionary<int, Character>();

        public CharacterManager()
        { }

        public void Dispose()
        { }

        public void Init()
        { }

        public void Clear()
        {
            this.Characters.Clear();
        }

        public Character AddCharacter(TCharacter cha)
        {
            Character character = new Character(SkillBridge.Message.CharacterType.Player, cha);
            EntityManager.Instance.AddEntity(cha.MapID, character);
            character.Info.EntityId = character.entityId;
            this.Characters[character.Id] = character;
            return character;
        }

        public void RemoveCharacter(int characterId)
        {
            if (!this.Characters.ContainsKey(characterId))
            {
                return;
            }

            var cha = this.Characters[characterId];
            EntityManager.Instance.RemoveEntity(cha.Data.MapID, cha);
            this.Characters.Remove(characterId);

            //// 讲道理，这里需要通知其他好友我下线了，也就是找到我的所有好友角色，刷新这些角色的好友列表
            //var friends = cha.Info.Friends;
            //foreach (var friend in friends)
            //{
            //    var id = friend.friendInfo.Id;
            //    Character character = CharacterManager.Instance.GetCharacter(id);
            //    if (character == null)
            //    {
            //        continue;
            //    }
            //    // 告诉我的每个朋友，我下线了
            //    character.friendManager.UpdateFriendInfo(cha.Info, 0);
            //}
        }

        public Character GetCharacter(int characterId)
        {
            Character character = null;
            this.Characters.TryGetValue(characterId, out character);
            return character;
        }
    }
}
