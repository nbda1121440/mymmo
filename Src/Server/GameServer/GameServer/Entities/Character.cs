﻿using Common;
using Common.Data;
using GameServer.Core;
using GameServer.Managers;
using GameServer.Models;
using Network;
// using GameServer.Managers;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    /// <summary>
    /// Character
    /// 玩家角色类
    /// </summary>
    class Character : CharacterBase, IPostResponser
    {

        public TCharacter Data;

        public ItemManager ItemManager;
        public QuestManager QuestManager;
        public StatusManager statusManager;
        public FriendManager friendManager;

        public Team team;
        public double TeamUpdateTS;

        public Guild guild;
        public double GuildUpdateTS;

        public Chat chat;

        public Character(CharacterType type, TCharacter cha) :
            base(new Core.Vector3Int(cha.MapPosX, cha.MapPosY, cha.MapPosZ), new Core.Vector3Int(100, 0, 0))
        {
            this.Data = cha;
            this.Id = cha.ID;
            this.Info = new NCharacterInfo();
            this.Info.Type = type;
            this.Info.Id = cha.ID;
            this.Info.EntityId = this.entityId;
            this.Info.Name = cha.Name;
            this.Info.Level = 10;//cha.Level;
            this.Info.ConfigId = cha.TID;
            this.Info.Class = (CharacterClass)cha.Class;
            this.Info.mapId = cha.MapID;
            this.Info.Gold = cha.Gold;
            this.Info.Entity = this.EntityData;
            this.Define = DataManager.Instance.Characters[this.Info.ConfigId];

            this.ItemManager = new ItemManager(this);
            this.ItemManager.GetItemInfos(this.Info.Items);

            this.Info.Bag = new NBagInfo();
            this.Info.Bag.Unlocked = this.Data.Bag.Unlocked;
            this.Info.Bag.Items = this.Data.Bag.Items;
            this.Info.Equips = this.Data.Equips;
            this.QuestManager = new QuestManager(this);
            this.QuestManager.GetQuestInfos(this.Info.Quests);
            this.statusManager = new StatusManager(this);
            this.friendManager = new FriendManager(this);
            this.friendManager.GetFriendInfos(this.Info.Friends);

            this.guild = GuildManager.Instance.GetGuild(this.Data.GuildId);

            this.chat = new Chat(this);
        }

        public long Gold
        {
            get { return this.Data.Gold; }
            set
            {
                if (this.Data.Gold == value)
                {
                    return;
                }
                this.statusManager.AddGoldChange((int)(value - this.Data.Gold));
                this.Data.Gold = value;
            }
        }

        public void PostProcess(NetMessageResponse message)
        {
            Log.InfoFormat("PostProcess > Character: characterID:{0}:{1}", this.Id, this.Info.Name);
            this.friendManager.PostProcess(message);
            if (this.team != null)
            {
                Log.InfoFormat("PostProcess > Team: characterID:{0}:{1} {2}<{3}", this.Id, this.Info.Name, TeamUpdateTS, this.team.Id);
                if (TeamUpdateTS < this.team.timestamp)
                {
                    TeamUpdateTS = team.timestamp;
                    this.team.PostProcess(message);
                }
            }
            if (this.guild != null)
            {
                Log.InfoFormat("PostProcess > Guild: characterID:{0}:{1} {2}<{3}", this.Id, this.Info.Name, GuildUpdateTS, this.guild.Name);
                //if (this.Info.Guild == null)
                //{
                    this.Info.Guild = this.guild.GuildInfo(this);
                    if (message.mapCharacterEnter != null)
                    {
                        GuildUpdateTS = guild.timestamp;
                    }
                    if (GuildUpdateTS < this.guild.timestamp && message.mapCharacterEnter == null)
                    {
                        GuildUpdateTS = guild.timestamp;
                        this.guild.PostProcess(this, message);
                    }
                //}
            }
            if (this.statusManager.HasStatus)
            {
                this.statusManager.PostProcess(message);
            }

            this.chat.PostProcess(message);
        }

        public void Clear()
        {
            // this.friendManager.UpdateFriendInfo(this.Info, 0);
            this.friendManager.OfflineNotify();
        }

        public NCharacterInfo GetBasicInfo()
        {
            return new NCharacterInfo()
            {
                Id = this.Id,
                Name = this.Info.Name,
                Class = this.Info.Class,
                Level = this.Info.Level
            };
        }
    }
}
